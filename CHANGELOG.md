# Changelog

## 0.1.2 - Small bugfixes

Tweaks to the way overlay tokens are handled in order to resolve some issues with the rendering of overlay statuses.  

## 0.1.1 - V11 support

Fixed issue caused by PIXI library update in V11 that caused icons to fail to render

## 0.1.0 - Initial Release

Override status icon rendering to render status icons in a circle around the token.

Currently a very rough version, which may not play nicely with other things that tweak token status rendering and so on.  

##